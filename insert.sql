INSERT INTO departamente_banca VALUES (1,'Contabilitate',2);
INSERT INTO departamente_banca VALUES(2,'IT',1);
INSERT INTO departamente_banca VALUES(3,'Financiar',2);
INSERT INTO locatii_banci VALUES(3,'Bucuresti','Timisoara');

INSERT INTO functii_banca VALUES(1,'Programator',3000);
INSERT INTO functii_banca VALUES(2,'Database Administrator',3250);
INSERT INTO functii_banca VALUES(3,'Contabil junior',2000);
INSERT INTO functii_banca VALUES(4,'Contabil senior',4000);
INSERT INTO functii_banca VALUES(5,'Intern',1800);
INSERT INTO functii_banca VALUES(7,'Economist',2200);
INSERT INTO functii_banca VALUES(8,'Manager',5500);
INSERT INTO functii_banca VALUES(9,'Call Support',1400);
INSERT INTO functii_banca VALUES(10,'Inginer',3500);
INSERT INTO functii_banca VALUES(11,'Analist Date',2800);

INSERT INTO angajati_banca VALUES(1,'Marinescu','Andreea',3860,'0741235699',2,1);
INSERT INTO angajati_banca VALUES(2,'Popescu','Liviu',4000,'0736148623',1,4);
INSERT INTO angajati_banca VALUES(3,'Anghel','Maria',2100,'0723486155',3,7);
INSERT INTO angajati_banca VALUES(4,'Martinescu','Silviu',1900,'0761549122',1,5);
INSERT INTO angajati_banca VALUES(5,'Gavril','Bogdan',8500,'0751346966',2,8);
INSERT INTO angajati_banca VALUES(6,'Stanescu','Sandra',4600,'0784651233',2,11);
INSERT INTO angajati_banca VALUES(7,'Titan','Codrin',4230,'0746859910',2,11);
INSERT INTO angajati_banca VALUES(8,'Florescu','Iulian',2150,'0765945866',3,5);

INSERT INTO Departamente_banca VALUES(4,'Vanzari',3);
INSERT INTO Functii_banca VALUES(12,'Agent Vanzari',1500);
INSERT INTO Angajati_banca VALUES(9,'Solomon','Martin',2300,'0742369411',4,12);
INSERT INTO Angajati_banca VALUES(10,'Barbulescu','Ioana',2550,'0741589455',4,12);
INSERT INTO Angajati_banca VALUES(11,'Gherasim','Sergiu',2100,'0761235477',4,12);

COMMIT;

INSERT INTO Clienti_Banca VALUES(1,1,'Margescu','Vlad','1981011035395');
INSERT INTO Clienti_Banca VALUES(2,9,'Voica','Andrei','1950613035481');
INSERT INTO Clienti_Banca VALUES(3,9,'Voica','Mariana','2801223034142');
INSERT INTO Clienti_Banca VALUES(4,10,'Simion','Elena','2990514015895');


INSERT INTO Conturi VALUES(1,1,'RO27RZBR9736859917493553',8000,'RON',TO_DATE('02-10-2010','DD-MM-YYYY'));
INSERT INTO Conturi VALUES(2,2,'RO35PORL7592719682934913',3500,'EUR',TO_DATE('30-11-2019','DD-MM-YYYY'));
INSERT INTO Conturi VALUES(3,3,'RO16RZBR3179314157492536',4800,'EUR',TO_DATE('28-03-2020','DD-MM-YYYY'));
INSERT INTO Conturi VALUES(4,3,'RO12RZBR4524199481771563',5500,'RON',TO_DATE('20-02-2014','DD-MM-YYYY'));
INSERT INTO Conturi VALUES(5,4,'RO35RZBR8562161182694335',6900,'RON',TO_DATE('27-02-2004','DD-MM-YYYY'));
INSERT INTO Conturi VALUES(6,1,'RO61PORL4145985235649361',7900,'USD',TO_DATE('17-12-2013','DD-MM-YYYY'));
INSERT INTO Conturi VALUES(7,3,'RO65PORL6755358555583265',3300,'USD',TO_DATE('21-05-2014','DD-MM-YYYY'));