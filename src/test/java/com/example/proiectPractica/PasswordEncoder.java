package com.example.proiectPractica;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class PasswordEncoder {

    public static void main(String[] args) throws IOException {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        System.out.println("Introduceti parola: ");
        Scanner scanner = new Scanner(System.in);
        String parola = scanner.nextLine();
        System.out.println("parola necriptata: " + parola);
       System.out.println("parola criptata: "+ bCryptPasswordEncoder.encode(parola));
    }
}
