package com.example.proiectPractica.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserController {

    @RequestMapping("/forbidden")
    public String noAccess()
    {
        return "forbidden";
    }

    @RequestMapping("/login_error")
    public String login_error()
    {
        return "login_error";
    }
}
