
CREATE FUNCTION procent_cl(p_idAng angajati_banca.id_ang%TYPE) RETURN NUMBER IS
p_clienti NUMBER;
total_clienti NUMBER;
BEGIN
 SELECT COUNT(id_ang) INTO total_clienti FROM Clienti_Banca;
 SELECT (COUNT(id_ang)*100)/total_clienti INTO p_clienti FROM Clienti_banca WHERE id_ang = p_idAng;
 RETURN p_clienti;
 EXCEPTION WHEN NO_DATA_FOUND THEN
  RETURN -1;
END;


CREATE OR REPLACE FUNCTION angajati_peste(p_numar NUMBER) RETURN SYS_REFCURSOR IS
angajati SYS_REFCURSOR;
BEGIN
 OPEN angajati FOR SELECT * FROM angajati_banca WHERE procent_cl(id_ang) > p_numar;
 RETURN angajati;
END;

CREATE OR REPLACE FUNCTION varsta_client(p_nume clienti_banca.nume%TYPE,p_prenume clienti_banca.prenume%TYPE) RETURN NUMBER AS
data_nastere DATE;
v_cnp Clienti_banca.CNP%TYPE;
varsta NUMBER;
BEGIN
 SELECT CNP INTO v_cnp FROM Clienti_banca WHERE UPPER(nume) = UPPER(p_nume) AND UPPER(prenume)= UPPER(p_prenume);
 data_nastere:=TO_DATE(SUBSTR(v_cnp,6,2) || '-' || SUBSTR(v_cnp,4,2) || '-' || SUBSTR(v_cnp,2,2),'dd-mm-rr'); 
 varsta:= TRUNC((SYSDATE-data_nastere)/365);
 RETURN varsta;
 EXCEPTION WHEN NO_DATA_FOUND THEN RETURN -1;
END;
 
CREATE OR REPLACE FUNCTION numar_total RETURN NUMBER IS
nr NUMBER;
BEGIN
 SELECT COUNT(id_client) INTO nr FROM clienti_banca;
 RETURN nr;
END;

CREATE OR REPLACE FUNCTION client_cu_varsta(p_varsta NUMBER) RETURN NUMBER IS
nr NUMBER;
BEGIN
 SELECT COUNT(*) INTO nr FROM clienti_banca
 WHERE varsta_client(nume,prenume) > p_varsta;
 RETURN nr;
END;
 
CREATE OR REPLACE FUNCTION total_conturi RETURN NUMBER IS
nr NUMBER;
BEGIN
 SELECT COUNT(*) INTO nr FROM Conturi;
 RETURN nr;
END;

CREATE OR REPLACE FUNCTION conturi_moneda(p_moneda Conturi.moneda%TYPE) RETURN NUMBER IS
nr NUMBER;
BEGIN
 SELECT COUNT(*) INTO nr FROM Conturi
 WHERE UPPER(moneda) = UPPER(p_moneda);
 RETURN nr;
END;

CREATE OR REPLACE FUNCTION procent_conturi_moneda(p_moneda Conturi.moneda%TYPE) RETURN NUMBER IS
procent NUMBER;
BEGIN
 SELECT (conturi_moneda(p_moneda)*100)/total_conturi INTO procent FROM dual;
 RETURN procent;
END;

CREATE OR REPLACE FUNCTION procent_conturi_an(p_an NUMBER) RETURN NUMBER IS
procent NUMBER;
conturi_an NUMBER;
total_conturi NUMBER;
BEGIN
 SELECT COUNT(*) INTO total_conturi FROM Conturi;
 SELECT COUNT(*) INTO conturi_an FROM Conturi WHERE EXTRACT(YEAR FROM DATA_START) = p_an;
 procent:=(conturi_an*100)/total_conturi;
 RETURN procent;
END;

CREATE OR REPLACE FUNCTION best_moneda RETURN VARCHAR2 IS
denumire Conturi.moneda%TYPE;
nr NUMBER;
BEGIN
 SELECT moneda INTO denumire FROM ( SELECT moneda, COUNT(*) nr FROM Conturi GROUP BY moneda ORDER BY nr DESC) 
       WHERE ROWNUM <2;
 RETURN denumire;
END;

CREATE OR REPLACE FUNCTION conturi_peste_medie RETURN NUMBER IS
medie NUMBER;
procent NUMBER;
total NUMBER;
depasite NUMBER;
BEGIN
 total := total_conturi;
 SELECT AVG(suma) INTO medie FROM Conturi;
 SELECT COUNT(*) INTO depasite FROM  Conturi WHERE suma > medie;
 procent:=(depasite*100)/total;
 RETURN procent;
END;

 


 